package tp2;

public class TestForme {

	public static void main(String[] args)
	{
		// TODO Auto-generated method stub
		/*************** Exercice1.4************/
		System.out.println("  " );
		System.out.println(" Exercice 1.4 " );
	    Forme f1 = new Forme();              // Instance f1 avec param�tre par d�fauts c'est-�-dire sans argument c
		Forme f2 = new Forme("vert", false); // Instance f2 avec couleur vert et coloriage faux avec argument 
		System.out.println(" f1 : "  + f1.getCouleur() + " - " +  f1.isColoriage()); // Couleur et coloriage par d�fauts du constructeur donc vert et vraie
		System.out.println(" f2 : " + f2.getCouleur() + " - "  + f2.isColoriage());
		//System.out.println(" f2 : " + f2.getCouleur() + "-" + f2.setColoriage(b));
		/***************Fin Exercice1.4************/	
		
		/*************** Exercice1.5************/	
		f1.setCouleur("rouge"); // Modification de la couleur
		f1.setColoriage(false); // Modification du booolean du coloriage

		System.out.println(" " );
		System.out.println(" Exercice 1.5 " );
		System.out.println(" Apr�s modification :  " );
		System.out.println(" f1 : "  + f1.getCouleur() + " - " +  f1.isColoriage());
		/*************** Fin Exercice1.5************/	
		
		/*************** Exercice 1.7 ************/	
		System.out.println("  " );
		System.out.println(" Exercice 1.7 " );

		System.out.println( f1.seDecrire()) ; 
		System.out.println(f2.seDecrire());
		/***************Fin  Exercice 1.7 ************/	
		
		/*************** Exercice 2.4 ************/	 // Cr�ation du cercle c1
		Cercle c1 = new Cercle() ; 
		/***************Fin  Exercice 2.4 ************/	
		
		/*************** Exercice 2. 5 ************/  // Affichage des caracteristique du cercle
		System.out.println("  " );
		System.out.println(" Exercice 2.5 " );
		System.out.println(" Un cercle de rayon " + c1.getRayon() + " est issue d'une forme de couleur " + c1.getCouleur() + " et de coloriage " + c1.isColoriage()) ; // Couleur orange car le constructeur par d�fauts d�finie une couleur orange
		/***************Fin  Exercice 2.5 ************/
		
		/***************Exercice 2.6************/ 
		System.out.println("  " );
		System.out.println(" Exercice 2.6  avec la m�thode seDecrire : " );
		System.out.println(c1.seDecrire());
		/***************Fin Exercice 2.6************/    		
		
		/***************Exercice 2.8************/ 
		System.out.println("  " );
		System.out.println(" Exercice 2.7 " );
		Cercle c2 = new Cercle(2.5) ;             // Creation du cercle c2 avec la valeur du rayon fix�  � 2.5 
		System.out.println(c2.seDecrire());	   // Affichage des caracteristique du cercle avec la m�thode se d�crire 
		/***************Fin Exercice 2.8************/    
		
		/***************Exercice 2.10 ************/
		System.out.println("  " );
		System.out.println(" Exercice 2.10 " );
		
		Cercle c3 = new Cercle(3.2,"jaune", false);  // Cr�ation du cercle c3 
		System.out.println(c3.seDecrire());
		/***************Fin Exercice 2.10 ************/
		
		/***************Exercice 2.11 ************/     //Programme de calcul d'aire des cercle c1 et c2 s
		System.out.println("  " );
		System.out.println(" Exercice 2.11 " );
	    System.out.println(" Cercle c1 :  " );
		System.out.println(" Aire: " + c1.calculerAire());
		System.out.println(" P�rimetre : " + c1.calculerPerimetre());
		System.out.println("  " );
		System.out.println(" Cercle c2 :  " );
		System.out.println(" Aire : " + c2.calculerAire());
		System.out.println(" P�rimetre : " + c2.calculerPerimetre());
		/***************Fin Exercice 2.11 ************/
		
		/***************Exercice 3.2  ************/    // Cr�ation du cylindre cy1 et description 
		System.out.println("  " );
		System.out.println(" Exercice 3.2 " );
		Cylindre cy1 = new Cylindre() ; 				// Cr�ation de l'objet cy1
		System.out.println(cy1.seDecrire());			// description de l'objet cy1
		/***************Fin Exercice 3.2  ************/
		
		/***************Exercice 3.4 ************/
		System.out.println("");
		System.out.println(" Exercice 3.4 " );
		Cylindre cy2 = new Cylindre(4.2,1.3,"bleu",true); // Cr�ation du nouveau cylindre avec les nouveau param�tre 
		System.out.println(cy2.seDecrire()); // Deescription du cylindree cy1              
		/***************Exercice 3.4 ************/
		
		/***************Exercice 3.5 ************/
		System.out.println("");
		System.out.println(" Exercice 3.5" );
		System.out.println(" Volume du cylindre cy1  : "  + cy1.calculevolumeCylindre() ) ; 
		System.out.println(" Volume du cylindre cy2 :  " + cy2.calculevolumeCylindre() );
		/***************Fin Exercice 3.5 ************/
		
		}

}



















