package tp3;

public class Brigand extends Humain 
{
	/***********Exercice 3.1***********/
	//Caracteristique du brigand 	
		private String look ; // Look du brigand     
		private int nbDame ; // Nombre de dame qu'il a kidnap� 
		private int  recompense ; 
	    private boolean prison = true  ; 
		private boolean libre  = false ; 
	/***********Fin Exercice 3.1***********/	
	/*********** Exercice 3.2***********/	// Constructeur pour cr�er le Brigand
		public Brigand(String n)  
		{
			super(n); //Appel du constructeur de la classe mere car le brigand est caracteris� par un noms 
			look = " mechant " ;  // Look du brigand
			prison =  false ;    // Il n'est pas en prison 
			recompense = 100 ;  // Ateend une valeur qui est de 100 $
			nbDame = 0 ; 
			boissonFav = " Cognac "; 
			}
	/***********Fin  Exercice 3.2***********/	
	/*********** Exercice 3.3**************/	 //M�thode pour recuperer la recompense
		int getRecompense()
		{
			return recompense ;     //retourne ici le montant de la r�compense 
		}
	/***********Fin  Exercice 3.3**************/		
	/***********Exercice 5.1***********/ // R�definitions de m�thode 
		String quelEestTonNoM() 
		{
			return   nom + " le "   + look  ;
		}
	/***********Exercice 5.1***********/ 	
	/***********Exercice 6.1***********/ //Ajout de la m�thode voidSePr�senter h�rit� de la classe M�re + Modification
	void sePresenter()
	{
		super.sePresenter();
		parler(" J'ai l'air " + look + " et j'ai enlev� " + nbDame);
		parler( " Ma t�te est mis � prix pour "  + recompense + " $ !!  "  );
	}
	/***********Fin Exercice 6.1***********/ 
	/***********Exercice 7.1************/	
	void enleve(Dame dame)			
	{
		libre = false  ; //Modification du statut libre de la femme
		nbDame = 1  ; 	 // Augmentation du nombre de dame enlever  
		recompense = 200  ; // On ajoute encore 100 encore  � la r�compense pr�vue  
		super.sePresenter();
		parler(" J'ai l'air " + look + " et j'ai enlev� " + nbDame);
		parler( " Ma t�te est mis � prix pour "  + recompense + " $ !!  "  );
		parler(" Ah ah ! " + dame.quelEestTonNoM()  + " , tu es ma prisoni�re  ! "); //dame.quelEestTonNom pour  
	}																	//voir le nom d�fini dans la classe Dame					
	/***********Fin Exercice 7.1***********/	
    /***********Exercice 8.2***********/	
	void  emprisonner(Sherif s1)						//M�thode dans laquelle un brigand peut se faire emprisonner par un sh�rif ici le sh�rif s1 
	{
		parler (" Damned, je suis fait ! "   + s1.quelEestTonNoM()  +   " ,  tu m�as eu ! ");  // Phrase exprim� par le brigand 
	}
	/***********Fin Exercice 8.2***********/	
}
