package tp3;

public class Humain 
{
	/*********** Exercice 1.1***********/	
	protected String nom;
	protected String boissonFav;
	/***********Fin Exercice 1.1***********/	
	/******Exercice 1.2**/
	public Humain(String n )
	{
		nom = n ;
		boissonFav = "lait" ; // Boisson favorite d'un Humain 
	}
	/*****Fin Exercice 1.2**/
	/******Exercice 1.3**/       //M�thode pour retourner le nom et la boisonFav
	String quelEestTonNoM()
	{
		return nom ; // Retourne le nom Humain 
	}
	String quelEstTaBoissonFavorite()
	{
		return boissonFav ; 
	}
	/***********Fin Exercice 1.3***********/	
	/***********Exercice 1.4***********/	
	void parler(String Texte) // M�thode ***PARLER*** qui affiche le nom de l'humain et le texte						
	{
		System.out.println(  nom  + " - " +  Texte ); 	// Le texte est d�fini dans l'exo 1.5 																
	}
	/***********Fin Exercice 1.4***********/	
	/***********Exercice 1.5************/	
	void sePresenter()
	{
		parler( " Bonjour je suis " + quelEestTonNoM() + " et ma boison favorite est le " + quelEstTaBoissonFavorite() ); 
	}
	/***********Fin Exercice 1.5***********/	
	/***********Exercice 1.6***********/	  //Ajout de la m�thode voidBoire
	void Boire()
	{
		parler( " - " +  " Ah ! un bon verre de "  + quelEstTaBoissonFavorite() +  " ! GLOUPS ! " );
	}

	/***********Fin Exercice 1.6***********/
}
