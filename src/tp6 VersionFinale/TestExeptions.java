package tp6;

public class TestExeptions {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*Exercice 1.1*/

		/*int x = 2, y = 0;
		System.out.println(x/y);
		System.out.println("Fin du programme");*/
		/* le programme ne fonctionne pas car on ne peut pas diviser par 0.*/

		/*Exercice 1.2*/
		/*int x = 2, y = 0;
		try{
			System.out.println(x/y);
		}
		catch (Exception e){
			System.out.println("Une exception a �t� captur�e");
		}
		System.out.println("Fin du programme");*/
		/* On constate qu'il n'y a plus l'erreur d'execution et que  le programme fonctionne */
		/*Exercice 1.3*/
		/*
		 * On affiche y/x = 0/5 = 0 ainsi le programme n'afficha pas d'erreur
		 */
		
		int x = 2, y = 0;
		try{
			System.out.println("y/x = " + y/x);
			System.out.println("x/y = " + x/y);
		}
		catch (ArithmeticException e){
			System.out.println("Une exception a �t� captur�e");
			System.out.println("Exception : " + e.getMessage());/*Exercice 1.5*/
			/*e.getMessage permet d'affcher le message d'erreur dans la console.*/
		}
		finally
		{
			System.out.println("Commande de fermmeture du programme");
		}
		System.out.println("Fin du programme");
	}
}


