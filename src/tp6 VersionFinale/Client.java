package tp6;

import java.io.PrintWriter;
import java.net.Socket;

public class Client {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		try
		{
			Socket client = new Socket("localhost", 8888);//D�claration de l'objet client 
			System.out.println("Client: " + client);//Affichage de l'adresse du client
			System.out.println("Le client s'est connecter");
			//Prinwriter pour formater la chaine de caracretre en don�es
			PrintWriter writer = new PrintWriter(client.getOutputStream());/*Ce qui permet au client d'envoyerdes �critures sous forme de texte.j'ai */
			System.out.println("Envoi du message : Hello World");
			writer.println("  Hello World  ");// Ce qui permet au client d'envoyer le message "hello world". 
			writer.flush(); //flush pour vider le buffer serveur
			client.close(); //Ferme connexion entre client et
		}
		catch(Exception e)
		{
			System.out.println("Exception : " + e.getMessage()); //Capture message d'erreur
		}
}
}
