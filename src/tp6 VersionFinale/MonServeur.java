package tp6;

import java.io.BufferedReader; 

import java.io.InputStreamReader;
import java.net.*;

public class MonServeur {
public static void main(String[] args) {
		// TODO Auto-generated method stub
		/********Exercice3.1************/
		try{
		//Try permet de tester						
			ServerSocket socketEcoute = new ServerSocket(8888); // Cr�ation du socket sur le port 8888.
			System.out.println("ServerSocket: " + socketEcoute);
		/********Fin Exercice3.1************/
		/********Exercice3.2************/
			Socket service = socketEcoute.accept();// le serveur accepte d'�couter le port 8888.
			System.out.println("Le client s'est connect�");
		/********Exercice3.2************/
			/********Exercice3.23************/
			BufferedReader bf = new BufferedReader( new InputStreamReader(service.getInputStream()));
			String message = bf.readLine();
			System.out.println("Message : " + message);// Ce qui permet de r��crire dans la console le message �cris dans le terminal.
			socketEcoute.close();/*Ce qui permet de fermer le serveur*/
		}
		catch (Exception e){//catch permet d'emprisonner les erreures. 
			System.out.println("Exception : " + e.getMessage());// Ce qui permet d'afficher le message d'erreur.
		}
}
}
