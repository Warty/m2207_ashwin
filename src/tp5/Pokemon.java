package tp4;

public class Pokemon 
{
		/***********Exercice 1.1***********/
		private int energie;
		private int maxEnergie;
		public String nom; 
		/***********FIN***********/	
		/***********Exercice 2.1**************/
		private int puissance ; 							//Ajout de l'attribut priv�e ataque 
		/*********** Fin  2.1***********/
		/***********Exercice 1.2*********/	
		//Constructeur
		public Pokemon(String n) // Constructeur qui prend en argument une chaine de caractere 
		{ 
			nom = n ; 									//nom du nouveau Pokemon 
			maxEnergie = 50 +(int)(Math.random()*((90-50)+1));
			energie = 30+(int)(Math.random()*((maxEnergie-30)+1)); //Random qui genere NbAlea
			puissance = 3+(int)(Math.random()*((10-3)+1));    //Exo 2.1 //G�n�ration de la puissance en un nombre al�atoire compris entre 3 et 10     	  
		}
		/***********FIN***********/	
		/***********Exercice 1.1***********/	
		//Accesseur
		public String getNom()
		{return nom;} 
		public int getEnergie()
		{return energie;}
		public int getPuissance()		// Pour l'exo 2.1 accesseur getPuissance
		{
			return puissance ; 
		}
		/***********FIN***********/	
		/***********Exercice 1.3***********/	
		public void sePresenter() 				//Pr�sentation du Pokemon  + ajout de la phrase puissance pour l'exo /****2.2****/
		{
			System.out.println(" Je suis "  + nom + " j'ai " + energie + " points d'�nergie " +  "(" + maxEnergie+ ")" + " max " + " et une puissance de   " + puissance );
		}
		/***********FIN***********/	
		/***********Exercice 1.5***********/	
		public void manger()					//M�thode qui va augmenter l'�nergie du Pokemon 	
		{
			int powerplus = 10+(int)(Math.random()*((30-10)+1)); // Le programme g�n�rer un nombre al�atoire entre 10 et 30 
															//qui d�finie en faite l'�nergie du Pokemon 
			if (powerplus+energie<=maxEnergie) 					
															
				energie+=powerplus;    							//On ajoute ici l'augmentation de l'�nergie cumul� � l'�nergie 
		}
		/***********Fin***********/	
		/***********Exercice 1.6***********/	
		public void vivre()									//M�thode qui va diminuer l'�nergie du Pokemon 
		{
			int vie =  20+(int)(Math.random()*((40-20)+1));  //G�n�ration d'un nombre al�atoire entre 20 et 40 
			
			if(energie - vie >=0)
				energie-=vie; 									//On ajoute la vie g�n�rer dans �nergie 
											// dans le cas ou l'�nergie est n�gatif 
		}															//L'�nergie du pok�mon ne pourra jamais �tre n�gative
		/***********FIN***********/	
		/***********Exercice 1.7***********/	
		public boolean isAlive()
		{
			if (energie == 0)
			{
				return false ;	// Si l'�negergie du pok�mon est null
			}
			else
			{ 
				return true ;    //Si l'�nergie du pokemon est positive 
			}
		}
		/***********Fin Exercice 1.7***********/	
		/***********Exercice 2.2***********/			//ajout de la m�thode perte d'�nergie 
		public void PerdreEnergie(int perte)
		{
			if(energie-perte >=0)				//Si la perte touch�  � l'�nergie est sup�rieur � 0 
				
			{
				if(energie <= 1/4 * maxEnergie) // Ajout de la condition if pour l'exo 4.2
				{
				energie-= perte*1.5;//Pour l'exo 4.2 on augmente la perte d'�nergie par 1.25 si energie est inf�rieur � 1/4 de MaxEnergie
				}
				
			  else
			  
				energie-=perte; //on soustrait la perte obtenu d'�nergie
			}
			else 
				energie=0; //Pour ne pas avoir d'�nergie n�gatif 
		}
		
		/***********Fin Exercice 2.2***********/		
		/***********Exercice 2.3***********/
		void attaquer(Pokemon adversaire )
		{
			adversaire.PerdreEnergie(puissance);  // adversaire qui perd de l'�nergie gr�ce a la m�thode perdre �negrie
			
			//Exo 3.1
			int fatigue = 0+(int)(Math.random()*((1-0)+1)); // g�n�ration de la fatigue en un nombre compris entre 0 et  1 
			if(puissance-fatigue>=1)/*la fatigue doit �tre inf�rieur ou �gale a 1*/
			puissance-=fatigue;			// On soustrait la fatigue g�n�re dans la puissance 
			//Fin Exo3.1
			else 
				{
				puissance=1;			//Si la puissance g�n�rer est inf�rieur  � 1// car elle doit �treau minimum � 1 
				}
			if(energie<=0.25*maxEnergie)			//Si l'�nergie du pokemon est inf�rieur ou �gal a  25 de max�nergie
			{
				adversaire.PerdreEnergie(puissance*2);	//La puissance des attaque est doubl� 
			}
			else
			{
				adversaire.PerdreEnergie(puissance);	//Si l'�nergie est sup�rieur � 1/4 de l'�nergie Max 
			}
	  }
		/***********Fin Exercice 2.3***********/
		/***********Fin***********/
}
		
