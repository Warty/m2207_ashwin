package tp5;
import java.awt.*;
 import java.applet.Applet;
 
 public class TestGridLayout extends Applet 
 {
	     public void init() 
     	{
         setLayout(new GridLayout(3,2));
         add(new Button( "Bouton 0 "));
         add(new Button(" Bouton 1 "));
         add(new Button(" Bouton 2 "));
         add(new Button(" Bouton 3"));
         add(new Button(" Bouton 4"));
         }
 }